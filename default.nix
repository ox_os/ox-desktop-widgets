{
  gitignore,
  pkgs,
  manifest,
  lib,
  dependencies,
  ...
}: (
  let
    getSrc                                        = src:
      let
        isGitIncluded = gitignore { basePath = src; extraRules = ''
          *
          !shell/
          !src/
          !.envrc.sh
          !default.nix
          !flake.nix
          !LICENSE
        ''; };
      in
        lib.cleanSourceWith {
          inherit src;
          name = "source";
          filter = isGitIncluded;
        };

    name                                                   = manifest.name;
    version                                                = manifest.version;
    description                                            = manifest.description;
    author                                                 = manifest.author;

    script                                                 = pkgs.writeShellScriptBin manifest.name (builtins.readFile ./src/index.sh);
  in (
    pkgs.stdenv.mkDerivation(
      {
        pname                                              = name;
        version                                            = version;

        src                                                = getSrc ./.;

        nativeBuildInputs                                  = with pkgs; [
          makeWrapper
          bash
        ] ++ dependencies;

        buildPhase                                         = ''
          mkdir -p "$out/bin"

          cp -r "./src" "$out/"

          # Main.
          cp "${script}/bin/${manifest.name}" "$out/bin/"
          chmod a+x "$out/bin/${manifest.name}"
        '';

        postInstall                                        = ''
          substituteInPlace "$out/src/eww/topbar/eww.yuck" \
            --replace '_PROGRAM_SRC_'         '${placeholder "out"}'

          substituteInPlace "$out/bin/${name}" \
            --replace '_PROGRAM_NAME_'        'PROGRAM_NAME="${name}"' \
            --replace '_PROGRAM_VERSION_'     'PROGRAM_VERSION="${version}"' \
            --replace '_PROGRAM_DESCRIPTION_' 'PROGRAM_DESCRIPTION="${description}"' \
            --replace '_PROGRAM_AUTHOR_'      'PROGRAM_AUTHOR="${author}"' \
            --replace '_PROGRAM_SRC_'         'PROGRAM_SRC="${placeholder "out"}"'

          wrapProgram "$out/bin/${name}" \
            --set PATH ${lib.makeBinPath (
              dependencies ++ [ "$out" ]
            )}
        '';
      }
    )
  )
)

