# <%{ name }%>

> <%{ description }%>

## Usage

```bash
<%{ asciinema }%>
```

<p align="center">
  <img src="./examples/demo.gif" />
</p>

## Eww

If you're looking for your own widgets check out `eww`.
I use a specific vetted version that's what this is - a wrapper.

## Development

```
source .envrc.sh
develop
help
```

## Contributions

| Author  | Estimated Hours |
| ------------- | ------------- |
<%#authors%>| [![<%name%>](https://github.com/<%name%>.png?size=64)](https://github.com/<%name%>) | <p align="right"><%hours%> Hours</p> |
<%/authors%>

