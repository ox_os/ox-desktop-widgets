{
  description                                                    = "OX Desktop Widgets Powered by Eww";

  inputs                                                         = {
    systems.url                                                  = "path:./flake.systems.nix";
    systems.flake                                                = false;

    nixpkgs.url                                                  = "github:Nate-Wilkins/nixpkgs/ox-unstable";

    flake-utils.url                                              = "github:numtide/flake-utils";
    flake-utils.inputs.systems.follows                           = "systems";

    gitignore.url                                                = "github:hercules-ci/gitignore.nix";
    gitignore.inputs.nixpkgs.follows                             = "nixpkgs";

    asciinema-automation.url                                     = "github:Nate-Wilkins/asciinema-automation/1.0.1";
    asciinema-automation.inputs.systems.follows                  = "systems";
    asciinema-automation.inputs.nixpkgs.follows                  = "nixpkgs";
    asciinema-automation.inputs.flake-utils.follows              = "flake-utils";

    fenix.url                                                    = "github:nix-community/fenix";
    fenix.inputs.nixpkgs.follows                                 = "nixpkgs";

    jikyuu.url                                                   = "github:Nate-Wilkins/jikyuu/1.0.1";
    jikyuu.inputs.systems.follows                                = "systems";
    jikyuu.inputs.nixpkgs.follows                                = "nixpkgs";
    jikyuu.inputs.flake-utils.follows                            = "flake-utils";
    jikyuu.inputs.fenix.follows                                  = "fenix";

    task-documentation.url                                       = "gitlab:ox_os/task-documentation/3.0.1";
    task-documentation.inputs.systems.follows                    = "systems";
    task-documentation.inputs.nixpkgs.follows                    = "nixpkgs";
    task-documentation.inputs.flake-utils.follows                = "flake-utils";
    task-documentation.inputs.gitignore.follows                  = "gitignore";
    task-documentation.inputs.fenix.follows                      = "fenix";
    task-documentation.inputs.asciinema-automation.follows       = "asciinema-automation";
    task-documentation.inputs.jikyuu.follows                     = "jikyuu";

    task-runner.url                                              = "gitlab:ox_os/task-runner";
    task-runner.inputs.systems.follows                           = "systems";
    task-runner.inputs.nixpkgs.follows                           = "nixpkgs";
    task-runner.inputs.flake-utils.follows                       = "flake-utils";
    task-runner.inputs.gitignore.follows                         = "gitignore";
    task-runner.inputs.fenix.follows                             = "fenix";
    task-runner.inputs.asciinema-automation.follows              = "asciinema-automation";
    task-runner.inputs.jikyuu.follows                            = "jikyuu";
    task-runner.inputs.task-documentation.follows                = "task-documentation";
  };

  outputs                                                        = {
    nixpkgs,
    flake-utils,
    gitignore,
    asciinema-automation,
    task-runner,
    task-documentation,
    ...
  }:
########################################################################################################################
#⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀#
#⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ Configuration ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀#
#⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀#
########################################################################################################################
    let
      mkPkgs                                        =
        system:
          pkgs: (
            # NixPkgs
            import pkgs { inherit system; }
            //
            # Custom Packages.
            {
              asciinema-automation                  = asciinema-automation.defaultPackage."${system}";
              task-documentation                    = task-documentation.defaultPackage."${system}";
            }
          );
    in (
      flake-utils.lib.eachDefaultSystem (system: (
        let
          pkgs                                      = mkPkgs system nixpkgs;
          manifest                                  = (pkgs.lib.importTOML ./package.toml);
          environment                               = {
            inherit system nixpkgs pkgs manifest;
            gitignore                               = gitignore.lib.gitignoreFilterWith;
            dependencies                            = with pkgs; [
              coreutils           # /bin/echo   /bin/date  /bind/head   /bin/ls  /bin/rm  /bin/cat
              bash                # /bin/bash
              which               # /bin/which
              ripgrep             # /bin/rg
              sd                  # /bin/sd
              gawk                # /bin/awk
              procps              # /bin/pkill
              alsa-utils          # /bin/amixer
              brightnessctl       # /bin/brightnessctl
              networkmanager      # /bin/nmcli
              hyprland            # /bin/hyprctl

              eww                 # /bin/eww
            ];
          };
          name                                      = manifest.name;
        in rec {
          packages.${name}                          = pkgs.callPackage ./default.nix environment;
          legacyPackages                            = packages;

          # `nix build`
          defaultPackage                            = packages.${name};

          # `nix run`
          apps.${name}                              = flake-utils.lib.mkApp {
            inherit name;
            drv                                     = packages.${name};
          };
          defaultApp                                = apps.${name};

          # `nix develop`
          devShells.default                         = import ./shell/default.nix (
            environment
          // {
            taskRunner                              = task-runner.taskRunner.${system};
          });
        }
      )
    )
  );
}


